﻿using System.Web.Configuration;

public class EmailViewModel
{
    public string senderName
    {
        get
        {
            return WebConfigurationManager.AppSettings["senderName"];
        }
    }

    public string senderEmail
    {
        get
        {
            return WebConfigurationManager.AppSettings["senderEmail"];
        }
    }
}