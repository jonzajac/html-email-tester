﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;

using System.Net;
using System.Net.Mail;

namespace EmailTester.Web.Controllers
{
    public class EmailController : Controller
    {
        //
        // GET: /Email
        public ActionResult Index()
        {
            return RedirectToAction("Create");
        }

        //
        // GET: /Email/Create

        public ActionResult Create()
        {
            EmailViewModel viewModel = new EmailViewModel();

            return View(viewModel);
        } 

        //
        // POST: /Email/Create

        [HttpPost, ValidateInput(false)]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                MailMessage msg = new MailMessage();

                MailAddress sender = new MailAddress(Request.Form["senderEmail"], Request.Form["senderName"]);
                MailAddress recipient = new MailAddress(Request.Form["toEmail"], Request.Form["toName"]);

                msg.To.Add(recipient);
                msg.From = sender;

                msg.Subject = Request.Form["subject"];

                msg.IsBodyHtml = true;

                msg.Body = Request.Form["message"];

                SmtpClient client = new SmtpClient(WebConfigurationManager.AppSettings["smtpHost"], 25);

                client.Send(msg);

                return View("Success");

            }
            catch (Exception e)
            {
                if(e is SmtpException)
                {
                    ViewBag.exception = e.Message;
                    ViewBag.innerException = e.InnerException.ToString();
                    return View("Error");
                }
                else
                {
                    ViewBag.exception = e;
                    ViewBag.innerException = String.Empty;
                    return View("Error");   
                }
                
            }
        }
        
    }
}
