# README

## What this is

This is a simple interface to make sending test HTML emails (during development) slightly less painful. 

## Requirements

  * Access to an SMTP server (I have used smtp4dev with great success - http://smtp4dev.codeplex.com/)
  * Machine with .NET 4 and IIS 7
  
## License 

Licensed under the WTFPL (http://www.wtfpl.net/), with two modifications: 

  * The Software shall be used for Good, not Evil.
  * The Software shall not be used for spam. 